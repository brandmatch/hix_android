-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:7:5
MERGED from FUNDRIVE_STUDIO:SweetAlert:unspecified:7:5
MERGED from FUNDRIVE_STUDIO:actionbarsherlock:unspecified:7:5
MERGED from FUNDRIVE_STUDIO:android-saripaar-master:unspecified:7:5
MERGED from FUNDRIVE_STUDIO:Cropper:unspecified:7:5
MERGED from FUNDRIVE_STUDIO:google-play-services_lib:unspecified:7:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:9:9
	android:minSdkVersion
		ADDED from AndroidManifest.xml:8:9
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:12:5
MERGED from FUNDRIVE_STUDIO:Library:unspecified:5:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:13:5
MERGED from FUNDRIVE_STUDIO:Library:unspecified:6:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.SET_WALLPAPER
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.SET_WALLPAPER_HINTS
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.WRITE_SETTINGS
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
uses-permission#android.permission.CHANGE_CONFIGURATION
ADDED from AndroidManifest.xml:18:5
	android:name
		ADDED from AndroidManifest.xml:18:22
uses-permission#android.permission.MODIFY_AUDIO_SETTINGS
ADDED from AndroidManifest.xml:19:5
	android:name
		ADDED from AndroidManifest.xml:19:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:20:5
	android:name
		ADDED from AndroidManifest.xml:20:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:21:5
	android:name
		ADDED from AndroidManifest.xml:21:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:23:5
	android:name
		ADDED from AndroidManifest.xml:23:22
uses-permission#com.google.android.c2dm.permission.RECEIVE
ADDED from AndroidManifest.xml:24:5
	android:name
		ADDED from AndroidManifest.xml:24:22
permission#com.apps.fundrive.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:26:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:28:9
	android:name
		ADDED from AndroidManifest.xml:27:9
uses-permission#com.apps.fundrive.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:30:5
	android:name
		ADDED from AndroidManifest.xml:30:22
application
ADDED from AndroidManifest.xml:32:5
MERGED from FUNDRIVE_STUDIO:SweetAlert:unspecified:11:5
MERGED from FUNDRIVE_STUDIO:actionbarsherlock:unspecified:11:5
MERGED from FUNDRIVE_STUDIO:android-saripaar-master:unspecified:11:5
MERGED from FUNDRIVE_STUDIO:Library:unspecified:8:5
	android:label
		ADDED from AndroidManifest.xml:35:9
	android:allowBackup
		ADDED from AndroidManifest.xml:33:9
	android:icon
		ADDED from AndroidManifest.xml:34:9
	android:theme
		ADDED from AndroidManifest.xml:36:9
activity#com.apps.fundrive.SuggestionAppsListActivity
ADDED from AndroidManifest.xml:39:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:43:13
	android:label
		ADDED from AndroidManifest.xml:44:13
	android:configChanges
		ADDED from AndroidManifest.xml:41:13
	android:theme
		ADDED from AndroidManifest.xml:42:13
	android:name
		ADDED from AndroidManifest.xml:40:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:45:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:46:17
	android:name
		ADDED from AndroidManifest.xml:46:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:48:17
	android:name
		ADDED from AndroidManifest.xml:48:27
activity#com.apps.fundrive.AdmobActivity
ADDED from AndroidManifest.xml:51:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:55:13
	android:configChanges
		ADDED from AndroidManifest.xml:53:13
	android:theme
		ADDED from AndroidManifest.xml:54:13
	android:name
		ADDED from AndroidManifest.xml:52:13
activity#com.apps.fundrive.WallMainActivity
ADDED from AndroidManifest.xml:57:9
	android:configChanges
		ADDED from AndroidManifest.xml:59:13
	android:name
		ADDED from AndroidManifest.xml:58:13
activity#com.apps.fundrive.MainActivitySlider
ADDED from AndroidManifest.xml:61:9
	android:configChanges
		ADDED from AndroidManifest.xml:63:13
	android:name
		ADDED from AndroidManifest.xml:62:13
activity#com.apps.fundrive.AuthonticationActivity
ADDED from AndroidManifest.xml:65:9
	android:configChanges
		ADDED from AndroidManifest.xml:67:13
	android:name
		ADDED from AndroidManifest.xml:66:13
activity#com.apps.fundrive.WallCategoryItemActivity
ADDED from AndroidManifest.xml:69:9
	android:configChanges
		ADDED from AndroidManifest.xml:71:13
	android:name
		ADDED from AndroidManifest.xml:70:13
activity#com.apps.fundrive.PinchZoom
ADDED from AndroidManifest.xml:77:9
	android:configChanges
		ADDED from AndroidManifest.xml:79:13
	android:name
		ADDED from AndroidManifest.xml:78:13
activity#com.apps.fundrive.SetAsWallpaperActivity
ADDED from AndroidManifest.xml:81:9
	android:configChanges
		ADDED from AndroidManifest.xml:83:13
	android:name
		ADDED from AndroidManifest.xml:82:13
activity#com.apps.fundrive.RingMainActivity
ADDED from AndroidManifest.xml:85:9
	android:configChanges
		ADDED from AndroidManifest.xml:87:13
	android:name
		ADDED from AndroidManifest.xml:86:13
activity#com.apps.fundrive.RingCategoryItemActivity
ADDED from AndroidManifest.xml:89:9
	android:configChanges
		ADDED from AndroidManifest.xml:91:13
	android:name
		ADDED from AndroidManifest.xml:90:13
activity#com.apps.fundrive.VideoMainActivity
ADDED from AndroidManifest.xml:93:9
	android:configChanges
		ADDED from AndroidManifest.xml:95:13
	android:name
		ADDED from AndroidManifest.xml:94:13
activity#com.apps.fundrive.VideoCategoryItemActivity
ADDED from AndroidManifest.xml:97:9
	android:configChanges
		ADDED from AndroidManifest.xml:99:13
	android:name
		ADDED from AndroidManifest.xml:98:13
activity#com.apps.fundrive.VideoSingle
ADDED from AndroidManifest.xml:101:9
	android:configChanges
		ADDED from AndroidManifest.xml:103:13
	android:name
		ADDED from AndroidManifest.xml:102:13
activity#com.apps.fundrive.AboutActivity
ADDED from AndroidManifest.xml:105:9
	android:configChanges
		ADDED from AndroidManifest.xml:107:13
	android:name
		ADDED from AndroidManifest.xml:106:13
activity#com.apps.fundrive.FavMainActivity
ADDED from AndroidManifest.xml:109:9
	android:configChanges
		ADDED from AndroidManifest.xml:111:13
	android:name
		ADDED from AndroidManifest.xml:110:13
activity#com.apps.fundrive.SignInActivity
ADDED from AndroidManifest.xml:113:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:116:13
	android:configChanges
		ADDED from AndroidManifest.xml:115:13
	android:name
		ADDED from AndroidManifest.xml:114:13
activity#com.apps.fundrive.SignUpActivity
ADDED from AndroidManifest.xml:118:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:121:13
	android:configChanges
		ADDED from AndroidManifest.xml:120:13
	android:name
		ADDED from AndroidManifest.xml:119:13
activity#com.apps.fundrive.ForgotPassActivity
ADDED from AndroidManifest.xml:123:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:126:13
	android:configChanges
		ADDED from AndroidManifest.xml:125:13
	android:name
		ADDED from AndroidManifest.xml:124:13
activity#com.apps.fundrive.Upload_Wallpaper
ADDED from AndroidManifest.xml:128:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:131:13
	android:configChanges
		ADDED from AndroidManifest.xml:130:13
	android:name
		ADDED from AndroidManifest.xml:129:13
activity#com.apps.fundrive.Upload_Ringtone
ADDED from AndroidManifest.xml:133:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:136:13
	android:configChanges
		ADDED from AndroidManifest.xml:135:13
	android:name
		ADDED from AndroidManifest.xml:134:13
activity#com.apps.fundrive.Upload_Video
ADDED from AndroidManifest.xml:138:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:141:13
	android:configChanges
		ADDED from AndroidManifest.xml:140:13
	android:name
		ADDED from AndroidManifest.xml:139:13
activity#com.apps.fundrive.Upload
ADDED from AndroidManifest.xml:143:9
	android:configChanges
		ADDED from AndroidManifest.xml:145:13
	android:name
		ADDED from AndroidManifest.xml:144:13
activity#com.apps.fundrive.DownloadMainActivity
ADDED from AndroidManifest.xml:147:9
	android:configChanges
		ADDED from AndroidManifest.xml:149:13
	android:name
		ADDED from AndroidManifest.xml:148:13
activity#com.apps.fundrive.SingleWallpaper
ADDED from AndroidManifest.xml:151:9
	android:configChanges
		ADDED from AndroidManifest.xml:153:13
	android:name
		ADDED from AndroidManifest.xml:152:13
activity#com.apps.fundrive.SingleRingtone
ADDED from AndroidManifest.xml:155:9
	android:configChanges
		ADDED from AndroidManifest.xml:157:13
	android:name
		ADDED from AndroidManifest.xml:156:13
activity#com.apps.fundrive.Activity_Feedback
ADDED from AndroidManifest.xml:159:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:162:13
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:163:13
	android:configChanges
		ADDED from AndroidManifest.xml:161:13
	android:name
		ADDED from AndroidManifest.xml:160:13
activity#com.apps.fundrive.Search_Wallpaper
ADDED from AndroidManifest.xml:165:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:168:13
	android:configChanges
		ADDED from AndroidManifest.xml:167:13
	android:name
		ADDED from AndroidManifest.xml:166:13
activity#com.apps.fundrive.Search_Ringtone
ADDED from AndroidManifest.xml:170:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:173:13
	android:configChanges
		ADDED from AndroidManifest.xml:172:13
	android:name
		ADDED from AndroidManifest.xml:171:13
activity#com.apps.fundrive.Search_Video
ADDED from AndroidManifest.xml:175:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:178:13
	android:configChanges
		ADDED from AndroidManifest.xml:177:13
	android:name
		ADDED from AndroidManifest.xml:176:13
activity#com.example.play.OpenYouTubePlayerActivity
ADDED from AndroidManifest.xml:181:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:185:13
	android:label
		ADDED from AndroidManifest.xml:184:13
	android:configChanges
		ADDED from AndroidManifest.xml:183:13
	android:theme
		ADDED from AndroidManifest.xml:186:13
	android:name
		ADDED from AndroidManifest.xml:182:13
activity#com.example.youtube.YoutubePlay
ADDED from AndroidManifest.xml:189:9
	android:configChanges
		ADDED from AndroidManifest.xml:191:13
	android:name
		ADDED from AndroidManifest.xml:190:13
activity#com.startapp.android.publish.list3d.List3DActivity
ADDED from AndroidManifest.xml:194:9
	android:theme
		ADDED from AndroidManifest.xml:197:13
	android:taskAffinity
		ADDED from AndroidManifest.xml:196:13
	android:name
		ADDED from AndroidManifest.xml:195:13
activity#com.startapp.android.publish.AppWallActivity
ADDED from AndroidManifest.xml:198:9
	android:configChanges
		ADDED from AndroidManifest.xml:200:13
	android:theme
		ADDED from AndroidManifest.xml:202:13
	android:taskAffinity
		ADDED from AndroidManifest.xml:201:13
	android:name
		ADDED from AndroidManifest.xml:199:13
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:204:9
	android:value
		ADDED from AndroidManifest.xml:206:13
	android:name
		ADDED from AndroidManifest.xml:205:13
activity#com.google.android.gms.ads.AdActivity
ADDED from AndroidManifest.xml:209:9
	android:configChanges
		ADDED from AndroidManifest.xml:211:13
	android:name
		ADDED from AndroidManifest.xml:210:13
receiver#com.example.gcm.GcmBroadcastReceiver
ADDED from AndroidManifest.xml:213:9
	android:permission
		ADDED from AndroidManifest.xml:215:13
	android:name
		ADDED from AndroidManifest.xml:214:13
intent-filter#com.apps.fundrive+com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:216:13
action#com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:217:17
	android:name
		ADDED from AndroidManifest.xml:217:25
category#com.apps.fundrive
ADDED from AndroidManifest.xml:219:17
	android:name
		ADDED from AndroidManifest.xml:219:27
service#com.example.gcm.GcmIntentService
ADDED from AndroidManifest.xml:223:9
	android:name
		ADDED from AndroidManifest.xml:223:18
android:uses-permission#android.permission.READ_PHONE_STATE
IMPLIED from AndroidManifest.xml:2:1 reason: com.daimajia.slider.library has a targetSdkVersion < 4
