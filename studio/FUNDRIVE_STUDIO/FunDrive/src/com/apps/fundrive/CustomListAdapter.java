package com.apps.fundrive;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter<String> {

	private final Activity mContext;
	private final ArrayList<String> mItemname;
	private final ArrayList<String> mDescription;
	private final ArrayList<Bitmap> mImage;

	public CustomListAdapter(Activity context, ArrayList<String> itemname, ArrayList<String> description,
			ArrayList<Bitmap> image) {
		super(context, R.layout.custom_app_list, itemname);
		// TODO Auto-generated constructor stub

		this.mContext = context;
		this.mItemname = itemname;
		this.mDescription = description;
		this.mImage = image;
	}

	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = mContext.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.custom_app_list, null, true);

		TextView txtTitle = (TextView) rowView.findViewById(R.id.app_title);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.app_icon);
		TextView txtDescription = (TextView) rowView.findViewById(R.id.app_description);

		txtTitle.setText(mItemname.get(position));

		imageView.setImageBitmap(mImage.get(position));
		txtDescription.setText("Description " + mDescription.get(position));
		return rowView;

	};
}