package com.apps.fundrive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class SuggestionAppsListActivity extends SherlockActivity {

	ArrayList<String> mItemname = new ArrayList();
	ArrayList<String> mDescription = new ArrayList();
	ArrayList<String> mLinks = new ArrayList();
	ArrayList<Bitmap> mImage = new ArrayList();
	int pos = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.suggestion_apps_list);

		mItemname.add("Fun Drive");
		mDescription.add("Continue to Fun Drive app");
		mLinks.add("");
		mImage.add(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon));
		new ParseTask().execute();
		CustomListAdapter adapter = new CustomListAdapter(SuggestionAppsListActivity.this, mItemname, mDescription,
				mImage);
		ListView list = (ListView) findViewById(R.id.list);
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Toast.makeText(getApplicationContext(), mItemname.get(position), Toast.LENGTH_SHORT).show();

				if (position == 0) {
					// Intent intentAdmob = new Intent(SuggestionAppsListActivity.this, AdmobActivity.class);
					Intent intentAdmob = new Intent(SuggestionAppsListActivity.this, RingMainActivity.class);

					startActivity(intentAdmob);
					finish();
				} else {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(mLinks.get(position)));
					startActivity(i);
				}

			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	private class ParseTask extends AsyncTask<Void, Void, String> {

		HttpURLConnection urlConnection = null;
		BufferedReader reader = null;
		String resultJson = "";

		@Override
		protected String doInBackground(Void... params) {
			// get data from external resources
			try {
				URL url = new URL("http://mobile.mxzone.net/data.json");

				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.connect();

				InputStream inputStream = urlConnection.getInputStream();
				StringBuffer buffer = new StringBuffer();

				reader = new BufferedReader(new InputStreamReader(inputStream));

				String line;
				while ((line = reader.readLine()) != null) {
					buffer.append(line);
				}

				resultJson = buffer.toString();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return resultJson;
		}

		@Override
		protected void onPostExecute(String strJson) {
			super.onPostExecute(strJson);

			JSONObject dataJsonObj = null;

			try {
				dataJsonObj = new JSONObject(strJson);
				JSONArray worldpopulation = dataJsonObj.getJSONArray("test");
				String[] picArr = new String[worldpopulation.length()];
				// scroll and output data of each app
				for (int i = 0; i < worldpopulation.length(); i++) {
					JSONObject world = worldpopulation.getJSONObject(i);

					String item = world.getString("name");
					String description = world.getString("description");
					String image = world.getString("image");
					String link = world.getString("link");

					mItemname.add(item);
					mDescription.add(description);
					mLinks.add(link);
					picArr[i] = image;

				}
				new LoadImage().execute(picArr);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private class LoadImage extends AsyncTask<String, String, Bitmap> {
		ProgressDialog pDialog;
		Bitmap bitmap;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SuggestionAppsListActivity.this);
			pDialog.setMessage("Loading ....");
			pDialog.show();
		}

		private void loadImageFromNetwork(String url, int i) {
			try {
				bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
				if (bitmap != null) {
					mImage.add(bitmap);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		protected Bitmap doInBackground(String... params) {
			try {
				for (int i = 0; i < params.length; i++) {
					loadImageFromNetwork(params[i], i);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(Bitmap bitmap) {

			if (bitmap != null) {
				pDialog.dismiss();
			} else {
				pDialog.dismiss();
				Toast.makeText(SuggestionAppsListActivity.this, "Image Does Not exist or Network Error",
						Toast.LENGTH_SHORT).show();
			}

			CustomListAdapter adapter = new CustomListAdapter(SuggestionAppsListActivity.this, mItemname, mDescription,
					mImage);
			ListView list = (ListView) findViewById(R.id.list);
			list.setAdapter(adapter);
		}
	}
}
