package com.example.adapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.apps.fundrive.DownloadPlayingSong;
import com.apps.fundrive.R;
import com.example.item.Song;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class DownloadRingAdapter extends BaseAdapter {

	Context mContext;
	int selectedPosition;
	private LayoutInflater songInf;
	private ArrayList songs;
	private Editor editor;
	
	
	private MediaPlayer mMediaPlayer;
	private ArrayList<Integer> mPlayingSong;
	private DownloadPlayingSong mPlayingSongStorage;
	
	public DownloadRingAdapter(Context context, ArrayList arraylist)
	{
		selectedPosition = 0;
		mContext = context;
		songs = arraylist;
		songInf = LayoutInflater.from(context);
		selectedPosition = mContext.getSharedPreferences("selectpref", 0).getInt("pos", 0);
		editor =mContext.getSharedPreferences("durationpref", 0).edit();
		
		mPlayingSongStorage = DownloadPlayingSong.get(mContext);
		
		mPlayingSong = mPlayingSongStorage.getPlayingSong();
		mMediaPlayer = mPlayingSongStorage.getMediaPlayer();
		
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
	    	    public void onCompletion(MediaPlayer mp) {
	    	    	mMediaPlayer.stop();
	    	    	mMediaPlayer.release();
	    	    	mMediaPlayer = null;
					
					mPlayingSongStorage.setMediaPlayer(mMediaPlayer);
					
					mPlayingSong.clear();
					mPlayingSongStorage.setPlayingSong(mPlayingSong);
					
					notifyDataSetChanged();
	    	    }
	    	});
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return songs.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getSelected()
	{
		return ((Song)songs.get(selectedPosition)).getPath();
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		final ViewHolder holder;
		final int item_pos = position;

		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.song, null);

			holder = new ViewHolder();

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.name=(TextView)view.findViewById(R.id.song_title);
		holder.artist=(TextView)view.findViewById(R.id.song_artist);
		holder.time=(TextView)view.findViewById(R.id.song_duration);
		holder.img_play=(LinearLayout)view.findViewById(R.id.playmusic_btn);
		holder.btn_img_play = (ImageView) view.findViewById(R.id.playmusic_btn_image);
		holder.rb=(RadioButton)view.findViewById(R.id.radiobutton);
		holder.rb.setVisibility(View.GONE);
		final Song currSong = (Song)songs.get(position);
		holder.name.setText(currSong.getTitle());
		holder.artist.setText(currSong.getArtist());
		long l = Long.parseLong(currSong.getDuration());
		String obj1 = String.valueOf((l % 60000L) / 1000L);
		String obj2 = String.valueOf(l / 60000L);
		if (obj1.length() == 1)
		{
			holder.time.setText((new StringBuilder("0")).append(((String) (obj2))).append(":0").append(((String) (obj1))).toString());
		} else
		{
			holder.time.setText((new StringBuilder("0")).append(((String) (obj2))).append(":").append(((String) (obj1))).toString());
		}
		
		if (mPlayingSong.contains(item_pos)) {
			holder.btn_img_play.setImageResource(R.drawable.stop_btndefault);
		} else {
			holder.btn_img_play.setImageResource(R.drawable.play_btndefault);
		}
		
		holder.img_play.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// File view1 = new File(currSong.getPath());
				// Log.d("fundrive", "mItemId: " + item_pos);

				if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
					mMediaPlayer.stop();
					mMediaPlayer.release();
					mMediaPlayer = null;

					if (mPlayingSong.contains(item_pos)) {
						mPlayingSong.clear();
						mPlayingSongStorage.setPlayingSong(mPlayingSong);
					} else {
						mPlayingSong.clear();
						mPlayingSongStorage.setPlayingSong(mPlayingSong);
						try {
							mMediaPlayer = new MediaPlayer();
							mMediaPlayer.setDataSource(currSong.getPath());
							mMediaPlayer.prepare();
						} catch (IOException e) {
							// yeah
						}
						
						mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				    	    public void onCompletion(MediaPlayer mp) {
				    	    	mMediaPlayer.stop();
				    	    	mMediaPlayer.release();
				    	    	mMediaPlayer = null;
								
								mPlayingSongStorage.setMediaPlayer(mMediaPlayer);
								
								mPlayingSong.clear();
								mPlayingSongStorage.setPlayingSong(mPlayingSong);
								
								notifyDataSetChanged();
				    	    }
				    	});
					
						mMediaPlayer.start();
						mPlayingSong.add(item_pos);
						mPlayingSongStorage.setPlayingSong(mPlayingSong);
					}
				} else {
					try {
						mMediaPlayer = new MediaPlayer();
						mMediaPlayer.setDataSource(currSong.getPath());
						mMediaPlayer.prepare();
					} catch (IOException e) {
						// yeah
					}
					
					mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			    	    public void onCompletion(MediaPlayer mp) {
			    	    	mMediaPlayer.stop();
			    	    	mMediaPlayer.release();
			    	    	mMediaPlayer = null;
							
							mPlayingSongStorage.setMediaPlayer(mMediaPlayer);
							
							mPlayingSong.clear();
							mPlayingSongStorage.setPlayingSong(mPlayingSong);
							
							notifyDataSetChanged();
			    	    }
			    	});

					mMediaPlayer.start();
					mPlayingSong.add(item_pos);
					mPlayingSongStorage.setPlayingSong(mPlayingSong);
				}

				mPlayingSongStorage.setMediaPlayer(mMediaPlayer);
				notifyDataSetChanged();
				
                // Intent intent = new Intent("android.intent.action.VIEW");
                // intent.setDataAndType(Uri.fromFile(view1), "audio/*");
                // mContext.startActivity(intent);
			}
		});


		return view;
	}

	public class ViewHolder {

		public TextView name,artist,time;
		LinearLayout img_play;
		ImageView btn_img_play;
		RadioButton rb;


	}

}
