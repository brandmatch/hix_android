package com.apps.fundrive;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

public class AdmobActivity extends SherlockActivity {
	InterstitialAd mInterstitialAd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading_ads_activity);
		getSupportActionBar().hide();
		
		ConnectivityManager cm = (ConnectivityManager) getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null == activeNetwork) {
			Toast.makeText(AdmobActivity.this, "Network Error",
					Toast.LENGTH_SHORT).show();
		//	Intent intentAdmob = new Intent(AdmobActivity.this, MainActivitySlider.class);
			Intent intentAdmob = new Intent(AdmobActivity.this, RingMainActivity.class);
			startActivity(intentAdmob);
			finish();
		}
		
		mInterstitialAd = new InterstitialAd(this);

		// set the ad unit ID
		mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

		AdRequest adRequest = new AdRequest.Builder().build();

		// Load ads into Interstitial Ads
		mInterstitialAd.loadAd(adRequest);

		mInterstitialAd.setAdListener(new AdListener() {
			public void onAdLoaded() {
				showInterstitial();
			}

			public void onAdClosed() {
				if (!mInterstitialAd.isLoaded()) {
			//		Intent intentAdmob = new Intent(AdmobActivity.this, MainActivitySlider.class);
					Intent intentAdmob = new Intent(AdmobActivity.this, RingMainActivity.class);
					startActivity(intentAdmob);
					finish();
				}
			}
		});
	}

	private void showInterstitial() {
		if (mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		}
	}
}