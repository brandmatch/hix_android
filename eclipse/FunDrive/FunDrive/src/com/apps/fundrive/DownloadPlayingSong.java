package com.apps.fundrive;

import android.content.Context;
import android.media.MediaPlayer;
import java.util.ArrayList;

public class DownloadPlayingSong {
    private static DownloadPlayingSong sPlayingSong;

    private Context mContext;
    
    private MediaPlayer mMediaPlayer;
    private ArrayList<Integer> mPlayingSong = new ArrayList<Integer>();


    public static DownloadPlayingSong get(Context context) {
        if (sPlayingSong == null) {
        	sPlayingSong = new DownloadPlayingSong(context);
        }
        return sPlayingSong;
    }

    private DownloadPlayingSong(Context context) {
        mContext = context.getApplicationContext();
    }

    public ArrayList<Integer> getPlayingSong() {
    	return mPlayingSong;
    }

    public void setPlayingSong(ArrayList<Integer> playingSong) {
        mPlayingSong = playingSong;
    }
    
    public MediaPlayer getMediaPlayer() {
    	return mMediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
    	mMediaPlayer = mediaPlayer;
    }

}
